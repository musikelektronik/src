.. role:: raw-html-m2r(raw)
   :format: html


Additive Synthese
=================

Was ist additive Synthese? Als eines der ältesten Syntheseverfahren kann additive Synthese kurz als eine Synthesetechnik beschrieben werden, die es ermöglicht, komplexere Wellenformen durch die Summierung einfacher Wellen, meist Sinuswellen, zu erzeugen.
Eine einfache summierte Wellenform (mit einem bestimmten harmonischen Spektrum) kann durch die Addition einer Anzahl von
harmonischen Frequenzen und eine proportionale Anpassung der Amplitude der einzelnen Frequenzen erstellt werden. Dies
kann in einer Histogrammdarstellung veranschaulicht werden, wobei eine zunehmende Reduzierung der Stärke der einzelnen
Frequenzen zu beobachten ist. In diesem Beispiel sind nur die ungeradzahligen Vielfachen der Grundfrequenz zu sehen,
wobei die Stärken der folgenden Frequenzen jeweils 1/3 der Grundfrequenz für die 3. Harmonik, 1/5 für die 5. Harmonik,
1/7 für die 7. usw. betragen:


.. image:: pix/add-synth-histogram.jpg
   :target: pix/add-synth-histogram.jpg
   :alt: Ein Histogram


Durch die sukzessive Addition einzelner Sinuswellen und die entsprechende Anpassung ihrer Stärken (wie oben beschrieben) können wir eine quasi-quadratische Wellenform generieren. Im Folgenden sind die einzelnen Schritte und eine mögliche Implementierung in Code dargestellt:


.. image:: pix/quasi-square.jpg
   :target: pix/quasi-square.jpg
   :alt: Quasi quadratisch


.. code-block::

   {Array.series(20, 100, 200).collect{arg f; SinOsc.ar(f, mul: (f/100).reciprocal)}.sum}.plot

Die Verläufe der einzelnen Frequenzen einer additiven Synthese müssen nicht notwendigerweise statisch sein. Sowohl die
Frequenz als auch die Amplitude (und die Phase) können sich im Laufe der Zeit verändern, um ein klanglich vielfältigeres
Ergebnis zu erzielen. Im folgenden Beispiel werden die Attack-Zeiten der 12 Partials (nur die ungeradzahligen Vielfachen
der Grundfrequenz) so verarbeitet, dass die höheren Frequenzen verhältnismäßig schneller ausklingen als die tieferen:

.. code-block::

   (
   var sz = 12;
   var startFreq = 50.rrand(100);
   var stepFreq = startFreq * 2;
   var freqs = Array.series(sz, startFreq, stepFreq);
   var dur = 0.5;
   var overallAmp = 0.5;
   {
       Line.kr(dur: dur, doneAction: 2);
       freqs.collect {
           arg frq, i;
           SinOsc.ar(
               freq: frq,
               mul: EnvGen.kr(
                   Env.new(
                       times: [0.01, dur * (i + 1).reciprocal]
                   ),
                   levelScale: (i + 1).reciprocal * overallAmp
               )
           )
       }.sum.dup(2)
   }.play
   )

Hörempfehlungen
---------------

:raw-html-m2r:`<iframe width="560" height="315" src="https://www.youtube.com/embed/GJ_DePDlDKI?si=J15h8wOygaxe6Ec8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`

:raw-html-m2r:`<iframe width="560" height="315" src="https://www.youtube.com/embed/fTU1v0bPRE4?si=bHuRsE1aO86ovda4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`

:raw-html-m2r:`<iframe width="560" height="315" src="https://www.youtube.com/embed/PcRVusjdO3Y?si=plK9bjU8TDGLXvpC" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`

:raw-html-m2r:`<iframe width="560" height="315" src="https://www.youtube.com/embed/0T-H-fVlHE0?si=uyRRSW4NeXq8p5GJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`
:raw-html-m2r:`<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/4basuUUatf8?si=slWfWdYwTuNNQCnw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe> -->`

Harmonische Reihe, Wellenform
-----------------------------

Die harmonische Reihe ist für den Charakter eines Klangs verantwortlich. Die Präsenz und Stärke jeder Oberschwingung bestimmen diesen Charakter. Bei gleicher Kraft und Spannung vibrieren kurze Saiten schneller als lange. Eine Saite, die normalerweise mit 100 Hz vibriert, halbiert sich (zum Beispiel durch Drücken des Fingers auf das Griffbrett an der Hälfte der Länge) und vibriert dann doppelt so schnell, nämlich mit 200 Hz. Wenn Sie sie auf ein Drittel ihrer ursprünglichen Länge verkürzen, indem Sie den Finger höher drücken, wird sie dreimal so schnell vibrieren oder 300 Hz. Eine Verkürzung auf ein Viertel erzeugt eine viermal schnellere Frequenz; 400 Hz. Durch solche Teilungen einer Saite entstehen Obertöne. Obertöne bilden die Grundlage für die Tonstufen der westlichen Musik. Die folgende Abbildung zeigt die Tonhöhen, die jeder oberen Oberschwingung entsprechen. Das tiefste C liegt bei etwa 65 Hz, dann folgt 130, dann 195, 260 (mittleres C), 325, 390, 455, 520, 585, 650, 715, 780, 845, 910, 975, 1040:


.. image:: pix/overtones-c65.jpg
   :target: pix/overtones-c65.jpg
   :alt: Obertöne des C 65Hz


Die meisten Saiten (und Klangkörper von Musikinstrumenten) schwingen gleichzeitig bei all diesen Frequenzen. Während eine Saite bei ihrer vollen Länge schwingt, schwingt sie auch in Hälften (siehe die Abbildung unten - beachten Sie, dass all diese Bewegungen gleichzeitig stattfinden):


.. image:: pix/vibrating-strings.jpg
   :target: pix/vibrating-strings.jpg
   :alt: Seiten Schwingungen


Diese halbe Schwingungsbewegung ist doppelt so schnell. Sie schwingt auch in Dritteln, Vierteln, Fünfteln usw. Eine einzelne Saite, die bei 65 Hz schwingt, erzeugt also auch die Frequenzen 130, 195 usw.

Die Klangfarbe besteht nicht nur aus der Präsenz, sondern auch aus der Amplitude dieser Obertöne. Jeder der Obertöne kann in einer Computerumgebung durch eine Sinuswelle repräsentiert werden. Als Faustregel haben die höheren Obertöne eine geringere Amplitude, jedoch variiert die tatsächliche Amplitude jedes Obertons je nach Klangfarbe. Die Präsenz und Amplitude der oberen Obertöne bilden den klanglichen Fingerabdruck eines jeden Klangs. Eine Violine kann beispielsweise einen stärkeren dritten Oberton, einen schwächeren vierten, keinen fünften und einen schwachen sechsten haben, während eine Posaune einen schwachen dritten, einen starken vierten und so weiter haben wird.

Unten sind Sonogramme verschiedener Musikinstrumente zu sehen, die jeweils zwei Noten spielen (Wiederholung derselben Note). Beachten Sie die unterschiedlichen Ansammlungen von Obertönen.

.. image:: pix/sonogram-harmonics.jpg
   :target: pix/sonogram-harmonics.jpg
   :alt: Sonogram Instrumente


Additive Synthese in Supercollider
----------------------------------

Als eine Aufgabe zur Additiven Synthese möchten wir eine Sägezahnwelle erstellen. Unten sehen Sie ein Sonogramm einer Sägezahnwelle und ihren Obertönen:

.. image:: pix/sonogram-sawtooth.jpg
   :target: pix/sonogram-sawtooth.jpg
   :alt: Sonogram Sawtooth

Um diesen Sägezahn zu rekonstruieren, benötigen wir einzelne Sinuswellen, die harmonisch sind oder Vielfache der Grundfrequenz. Hier ist eine erste grobe Version, aufgebaut auf einer Grundfrequenz von 200. Das Hinzufügen von 12 Sinuswellen würde normalerweise zu Verzerrungen führen, daher werden sie alle um 0,1 skaliert.

.. code-block::

   (
   {
       (
           SinOsc.ar(200) + SinOsc.ar(400) + SinOsc.ar(600) +
           SinOsc.ar(800) + SinOsc.ar(1000) + SinOsc.ar(1200) +
           SinOsc.ar(1400) + SinOsc.ar(1600) + SinOsc.ar(1800) +
           SinOsc.ar(2000) + SinOsc.ar(2200) + SinOsc.ar(2400)
       ) * 0.1
   }.scope
   )

Und hier unternehmen wir noch eine Modifikation: Wir justieren die Amplituden der einzelnen Wellenschichten (Obertöne) individuell und proportional zu den Obertonnummern. Das bedeutet, der zweite Oberton ist halb so stark wie die Grundfrequenz, der dritte ein Drittel so stark usw.

.. code-block::

   (
   {
       (
           SinOsc.ar(200: mul: 1) + SinOsc.ar(400, mul: 1/2) +
           SinOsc.ar(600, mul: 1/3) +
           SinOsc.ar(800, mul: 1/4) + SinOsc.ar(1000, mul: 1/5) +
           SinOsc.ar(1200, mul: 1/6) +
           SinOsc.ar(1400, mul: 1/7) + SinOsc.ar(1600, mul: 1/8) +
           SinOsc.ar(1800, mul: 1/9) +
           SinOsc.ar(2000, mul: 1/10) + SinOsc.ar(2200, mul: 1/11) +
           SinOsc.ar(2400, mul: 1/12)
       ) * 0.1
   }.scope
   )

In der nächsten Version überlassen wir die Berechnung der Obertöne dem Computer: Dafür nutzen wir eine Variable für die Grundfrequenz und multiplizieren diese einfach mit den Obertonnummern. Wir packen die Obertöne außerdem in ein Array, um sie zwischen den ersten 12 Ausgangsbussen zu verteilen. Auch wenn wir wahrscheinlich keine 12 Ausgänge an unserem Rechner angeschlossen haben, können wir diese im Stethoskop gut betrachten.

.. code-block::

   (
   {
       var f = 200; // Die Grundfrequenz
       [
           SinOsc.ar(f*1: mul: 1) + SinOsc.ar(f*2, mul: 1/2) +
           SinOsc.ar(f*3, mul: 1/3) +
           SinOsc.ar(f*4, mul: 1/4) + SinOsc.ar(f*5, mul: 1/5) +
           SinOsc.ar(f*6, mul: 1/6) +
           SinOsc.ar(f*7, mul: 1/7) + SinOsc.ar(f*8, mul: 1/8) +
           SinOsc.ar(f*9, mul: 1/9) +
           SinOsc.ar(f*10, mul: 1/10) + SinOsc.ar(f*11, mul: 1/11) +
           SinOsc.ar(f*12, mul: 1/12)
       ]
   }.scope(numChannels: 12)
   )

???

.. code-block::

   (
   {
       var speed = 14; // Amplitudenveränderungsgeschwindigkeit
       var f = 200; // Die Grundfrequenz
       Mix.new([
           SinOsc.ar(f*1: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/1),
           SinOsc.ar(f*2: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/2),
           SinOsc.ar(f*3: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/3),
           SinOsc.ar(f*4: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/4),
           SinOsc.ar(f*5: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/5),
           SinOsc.ar(f*6: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/6),
           SinOsc.ar(f*7: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/7),
           SinOsc.ar(f*8: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/8),
           SinOsc.ar(f*9: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/9),
           SinOsc.ar(f*10: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/10),
           SinOsc.ar(f*11: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/11),
           SinOsc.ar(f*12: mul: LFNoise1.kr(rrand(speed, spped*2), 0.5, 0.5)/12),
       ]) * 0.5
   }.scope(numChannels: 1)
   )

**Übung** Versuchen Sie, im obigen Beispiel die Geschwindigkeit der Amplitudenänderung ``speed`` zu variieren. Beobachten Sie, wie sich entsprechend die Klangfarbe verhält. Ersetzen Sie außerdem die Grundfrequenz durch einen anderen *Controller*\ , wie zum Beispiel SinOsc, die Maus oder ein LFNoise, und beobachten Sie die Veränderungen im Klang.

Als Nächstes werden wir eine Hüllkurve hinzufügen. Wir könnten die letzten Multiplikatoren durch eine einzelne Hüllkurve ersetzen, um die Amplitude aller Oszillatoren gleichzeitig zu steuern. Und so machen es die meisten Synthesizer mit begrenzten Modulen. Da wir jedoch mit Code arbeiten, können wir ganz einfach jedem Oszillator eine eigene Hüllkurve zuweisen, wodurch jede Harmonische unabhängig wird und das Ergebnis ein natürlicherer Klang ist.

.. code-block::

   (
   {
       var g = Impulse.kr(1/3); // Hüllkurvenschranke (gate)
       var f = 200; // Die Grundfrequenz
       Mix.new([
           SinOsc.ar(f*1: mul: EnvGen.kr(Env.perc(releaseTime: 1.4), g)/1),
           SinOsc.ar(f*2: mul: EnvGen.kr(Env.perc(releaseTime: 1.1), g)/2),
           SinOsc.ar(f*3: mul: EnvGen.kr(Env.perc(releaseTime: 2), g)/3),
           SinOsc.ar(f*4: mul: EnvGen.kr(Env.perc(releaseTime: 1), g)/4),
           SinOsc.ar(f*5: mul: EnvGen.kr(Env.perc(releaseTime: 1.8), g)/5),
           SinOsc.ar(f*6: mul: EnvGen.kr(Env.perc(releaseTime: 2.9), g)/6),
           SinOsc.ar(f*7: mul: EnvGen.kr(Env.perc(releaseTime: 4), g)/7),
           SinOsc.ar(f*8: mul: EnvGen.kr(Env.perc(releaseTime: 0.3), g)/8),
           SinOsc.ar(f*9: mul: EnvGen.kr(Env.perc(releaseTime: 1), g)/9),
           SinOsc.ar(f*10: mul: EnvGen.kr(Env.perc(releaseTime: 3.6), g)/10),
           SinOsc.ar(f*11: mul: EnvGen.kr(Env.perc(releaseTime: 2.3), g)/11),
           SinOsc.ar(f*12: mul: EnvGen.kr(Env.perc(releaseTime: 1.1), g)/12),
       ]) * 0.5
   }.scope(numChannels: 1)
   )

Unten sind ein paar Hinweise, wie wir den oben stehenden (langatmigen) Code etwas kompakter ausdrücken könnten. Wenn Sie sich an die Multi-Array-Expansion vom Wintersemester erinnern, wissen Sie bereits, dass ein UGen mit einem Array-Argument zu einem Array mit Kopien des UGens und entsprechenden Parametern für jedes UGen expandiert wird. Das Mix-UGen hingegen kombiniert alle UGens in seinem Array-Argument zusammen, sodass wir am Ende aus einem Array von UGens ein einziges Signal (die Summe der UGens) erhalten. Mit diesem Wissen kann man erkennen, dass folgende Zeile:

.. code-block::

   Mix.new([SinOsc.ar(200), SinOsc.ar(400), SinOsc.ar(600)])

auch so geschrieben werden kann:

.. code-block::

   Mix.new(SinOsc.ar([200, 400, 600]))

Hier sind zur Wiederholung ein paar weitere Techniken, wie wir bequem Arrays erstellen können:

.. code-block::

   ```

   Und dementsprechend können wir das Additive-Synthese-Beispiel oben wie folgt ausdrücken:

(
{
    f = 100;
    t = Impulse.kr(1/3);
    Mix.new(
        SinOsc.ar(
            f\ *(1..12),
            mul: EnvGen.kr(
                Env.perc(0, 1),
                t,
                levelScale: 1/(1..12),
                // könnte auch geschrieben werden als: rrand(1.0, 3.0).dup(12)
                timeScale: [1.4, 1.1, 2, 1, 1.8, 2.9, 4, 0.3, 1, 3.6, 2.3, 1.1]
            )
        )
    ) * 0.5
}.scope(1)
)

.. code-block::

   Zum Abschluss und zur Unterhaltung erstellen wir zwei Varianten, bei denen wir die einzelnen Wellen zufällig zwischen dem linken und rechten Kanal (oder auch irgendwo dazwischen) verteilen. Dies geschieht mit dem `Pan2` UGen. Führen Sie die folgenden Blocks mehrmals aus, um den Unterschied zu hören:

(
var knums = [ 55, 56, 59, 60, 62, 63, 65, 67, 68, 71, 72 ];
{
    Mix.new(
        Pan2.ar(
            SinOsc.ar(
                freq: knums.midicps,
                mul: LFNoise1.kr(rrand(0.1, 0.5).dup(8), 0.5, 0.5)
            ),
            {1.0.rand2}.dup(knums.size)
        )
    ) * 0.2
}.play
)

(
var harmonics = 16, fund = TChoose.kr(Dust.kr(0.5), Array.series(6, 30, 7));
{
    Mix.fill(
        harmonics,
        {
            arg count;
            Pan2.ar(
                FSinOsc.ar(
                    fund * (count + 1),
                    mul: FSinOsc.kr(rrand(1/3, 1/6), mul: 0.5, add: 0.5 )
                ),
                1.0.rand2)
        }
    ) / (2 * harmonics)
}.play;
)

.. code-block::


   ## Nicht harmonische Spektren (inharmonische Spektren)
   In den obigen Beispielen ist jede der Sinusschwingungen ein Vielfaches des Grundtons (f, 2f, 3f, 4f usw.). Dies ergibt ein harmonisches Spektrum. Die meisten gestimmten Instrumente haben harmonische Spektren. Andere Instrumente hingegen wie Gongs, Glocken und Becken tendieren zu inharmonischen Spektren, d.h. zu einer Reihe von Frequenzen, die keine Vielfachen des Grundtons sind. Um ein inharmonisches Spektrum zu erzeugen, müssen für jede Sinusschwingung nicht zusammenhängende Werte eingegeben werden, z. B. 135, 173, 239, 267, 306, 355, 473, 512, 572 und 626. In den obigen Beispielen wurde die Amplitude jedes oberen Obertons in Bezug auf seine Position berechnet; höhere Obertöne waren leiser. Bei inharmonischen Spektren haben die Amplituden jedoch kein Muster. Zum Beispiel 0,25, 0,11, 0,12, 0,04, 0,1, 0,15, 0,05, 0,01, 0,03, 0,02 und 0,12. Hier ist ein Patch, der die Frequenzen und Amplituden aus meinem zufälligen Prozess zusammenaddiert. Es gibt eine Hüllkurve, daher erhalten Sie das Gefühl einer Glocke, und es hat eine sehr metallische Resonanz:

(
{
    Mix.new(
        Pan2.ar(
            SinOsc.ar(
                freq: Array.exprand(16, 40, 4000),
                mul: Array.rand(16, 0.1, 1),
            ) *
            EnvGen.kr(Env.perc(releaseTime: 4), levelScale: 0.15, doneAction: 2),
            1.0.rand2
        )
    )
}.play
)

.. code-block::


   **Übung** Definieren Sie einen Synthesizer, der den oben beschriebenen Klang der Additivsynthese hat. Verwenden Sie dann den Synthesizer, um verschiedene Dur-/Mollakkorde zu spielen.
   (Tipp: die Funktion `midicps` wandelt eine MIDI-Nummer in die entsprechende Frequenz um.)

   ## Zufall in SC???

   ## Eine Glocke
   Im folgenden Beispiel führen wir ein Experiment mit Zufallsfrequenzen eines nicht-harmonischen Klangs durch. Wir werden auch ein einfaches Modell für einen Übergang vom harmonischen in das inharmonische des gleichen Klangs implementieren. Dafür modulieren wir alle Obertöne unseres Klangs, jeweils zwischen dem 1- und einem X-fachen dessen Originalform (eine Vielfache der Grundfrequenz), wobei X eine zufällige Zahl ist. Des Weiteren legen wir die folgenden Parameter als Zufallsdaten fest: 

   - die Anzahl der Obertöne 
   - die Release-Time der einzelnen Obertöne

   Die Obertöne werden mithilfe des Mix UGens zu einem einzigen Signal gemischt.
   Führen Sie den untenstehenden Code mehrfach aus und analysieren Sie ihn. Verändern Sie auch die Parameter und beobachten Sie die Unterschiede im Ergebnis.

(
{
    var trigger, partials, fund;
    trigger = Dust.kr(2/4);
    partials = partials = 5.rrand(16);
    fund = exprand(50, 700);
    Mix.fill(
        partials,
        {
            arg count;
            SinOsc.ar(
                (count + 1) * fund * MouseX.kr(1.0, 0.25.rrand(4))
            ) * EnvGen.kr(
                Env.perc(0.01, rrand(0.2, 3.0)),
                trigger,
                1.0.rand
            )
        }
    ) / partials ! 2
}.play
)

.. code-block::


   ## Code Beispiele
   Es folgen einige Synthesebeispiele, die unter Verwendung der Additiven Synthese erstellt wurden.
   ### Beispiel 1

(
{
    var trigger, fund;
    trigger = Dust.kr(3/7);
    fund = rrand(100, 400);
    Mix.new(
        Array.fill(
            16, // Anzahl Partialtöne
            {
                arg counter;
                var partial;
                partial = counter + 1;
                Pan2.ar(
                    // Signal
                    SinOsc.ar(fund * partial) *
                    EnvGen.kr(Env.adsr(0, 0, 1.0, 5.0), trigger, partial.reciprocal ) *
                    max(0, LFNoise1.kr(rrand(5.0, 12.0))),
                    // Pan pos
                    1.0.rand2
                )
            }
        )
    ) * 0.5 // Gesamtlautstärke
}.play
)

.. code-block::

   ### Beispiel 2
   Einige des oben definierten Synths wurden gemischt:

(
{
    var trigger, fund, flashInst;
    var flashCount = 7;
    flashInst = Array.fill(
        flashCount,
        {
            trigger = Dust.kr(5/7);
            fund = Array.geom(flashCount, 50, 1.5).choose;
            Pan2.ar(
                Mix.new(
                    Array.fill(
                        16,
                        {
                            arg counter;
                            var partial;
                            partial = counter + 1;
                            SinOsc.ar(fund * partial) *
                            EnvGen.kr(Env.adsr(0, 0, 1.0, 5.0), trigger, 1/partial) *
                            max(0, LFNoise1.kr(rrand(2.0, 12.0)))
                        }
                    )
                ) * flashCount.reciprocal,
                // Pan pos
                1.0.rand2
            )
        }
    );
    Mix.new(flashInst)
}.play
)

.. code-block::

   ### Beispiel 3

(
{
    var harmonics = 16, fund = 50, speeds;
    speeds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] / 5;
    Mix.fill(
        harmonics,
        {
            arg count;
            Pan2.ar(
                FSinOsc.ar(
                    fund * (count + 1),
                    mul: max(0, FSinOsc.kr(speeds.wrapAt(count))) * harmonics.reciprocal
                ),
                1.0.rand2
            )
        }
    )
}.play;
)

.. code-block::

   ### Beispiel 4

(
{
    var harmonics = 16, fund, speeds;
    speeds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11] / 20;
    fund = (Lag3.kr(MouseX.kr(20, 50).round(7), 1)).midicps;
    Mix.fill(
        harmonics,
        {
            arg count;
            Pan2.ar(
                FSinOsc.ar(
                    fund * (count + 1),
                    mul: max(0, FSinOsc.kr(speeds.wrapAt(count))) * harmonics.reciprocal
                ),
                1.0.rand2
            )
        }
    )
}.play;
)

.. code-block::

   ## Übungen
   ### Übung 1
   Im Patch unten ersetzen Sie 400 und 1/3 durch Arrays von Frequenzen und 0.3 durch ein Array von Pan-Positionen. Sie können sie entweder ausschreiben oder `{rrand(?, ?)}.dup(?)` verwenden.

(
{
    Mix.ar(
        Pan2.ar(
            SinOsc.ar(
                400, // Freq
                mul: EnvGen.kr(
                    Env.perc(0, 2),
                    Dust.kr(1/3) // Trigger density
                ) * 0.1
            ), 0.3 // Pan position
        )
    )
}.play
)
```

Übung 2
^^^^^^^

Verwenden Sie ``Array.fill``\ , um ein Array mit 20 zufälligen Werten aus aufeinanderfolgenden Bereichen von 100 zu erstellen, sodass der erste Wert zwischen 0 und 100 liegt, der nächste zwischen 100 und 200, der nächste zwischen 200 und 300 usw. Das Ergebnis sollte etwas Ähnliches wie [67, 182, 267, 344, 463, 511 usw.] sein. Verwenden Sie Ihr Array als Frequenzen in Übung 1.


Musikelektronik
===============

Modul Kernfach Theorie III
--------------------------

Das Ziel dieser Übung ist das Kennenlernen und Erproben der Möglichkeiten der elektronischen Musik in Theorie und Praxis anhand eines gemeinsam zu erarbeitenden Musikbeispiels. Die Analyse von Werken aus dem Repertoire ist ebenfalls ein Schwerpunkt dieses Seminars.


* Titel (dt.) der konkreten Lehrveranstaltung: Elektronische Musik + Musik-Elektronik
* Veranstaltungsart: Seminar/Übung (anmeldepflichtig)
* Semesterwochenstunden (SWS): 2
* Leistungspunkte (ECTS): 2

Das Modul beginnt jeweils im Wintersemester und erstreckt sich über zwei Semester. Der Unterricht findet regulär als Blockseminar im zweiwöchentlichen Rhythmus statt. Wegen der genauen Termine und Zeiten bitte eine Anfrage an: Amir.Teymuri@hmtm.de

.. toctree::
   :maxdepth: 2

   grundbegriffe
   ws
   ss
   uebungen
   repertoire

Grundlegende periodische Wellenformen
=====================================

In den folgenden Seiten werden u.a. die folgenden vier UGens immer wieder eingesetzt. Da deren Namen in SuperCollider von den herkömmlichen Wellenformen, die sie produzieren, abweichen, hier zur Referenz die vier elementaren periodischen `Wellenformen <https://de.wikipedia.org/wiki/Waveform>`_ und wie sie in SC erstellt werden:

* Sinusschwingung: ``SinOsc``
* Rechteckschwingung: ``Pulse``
* Dreieckschwingung: ``VarSaw``
* Sägezahnschwingung: ``Saw``

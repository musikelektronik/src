# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Musikelektronik'
copyright = '2024, Amir Teymuri'
author = 'Amir Teymuri'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
exclude_patterns = []

language = 'de'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'bizstyle'
# html_theme = 'insegel'
# html_theme = 'traditional'
# import sphinx_adc_theme
# html_theme = 'sphinx_adc_theme'
# html_theme_path = [sphinx_adc_theme.get_html_theme_path()]

html_static_path = ['_static']


Abspielen der N ersten Partialtöne
==================================

Hier ist eine einfache Instrument-Definition:

.. code-block::

   (
    SynthDef(\instrument, {
        arg freq=220, amp=0.5, dur=1;
        var env = EnvGen.kr(Env.perc(attackTime: 0, releaseTime: dur));
        var sig = SinOsc.ar(freq: freq, mul: amp * env);
        Out.ar(0, sig.dup)
    }).add
    )

die wir z.B. folgendermaßen spielen können

.. code-block::

    Synth(\instrument, [\freq, 330, amp: 0.25, dur: 4])

Verwendet Task- oder Routine-Klassen, um eine aufsteigende Sequenz von Obertönen basierend auf der Grundfrequenz von 100 Hertz abzuspielen.
Die Dauer der einzelnen Obertöne könnt ihr selbst bestimmen.
Unten steht eine Vorlage, die ihr benutzen könnt:

.. code-block::

   (
    var grundFreq = ?; // Die Grundfrequenz
    var numPartials = ?; // Anzahl der Partialtöne
    var dur = ?; // Dauer der einzelnen Partialtöne
    Task({
        numPartials.do({
            // Achtet darauf dass Iterationen mit der Zahl 0 beginnen
            arg partial;
            // Hier muss das Instrument zum Klingen kommen...

            // Danach warten wir für die bestimmte Zeit
            dur.wait
        })
    }).play
    )


Subtraktive Synthese
====================

Beim subtraktiven Syntheseverfahren gehen wir den entgegengesetzten Weg zum additiven Syntheseverfahren. Wir formen unseren Klang, indem wir selektiv Daten aus einem komplexen Klangmaterial herausfiltern. Eine solche Klangquelle bildet das Rauschen. Zuerst betrachten wir die Charakteristik des Rauschens genauer.

Noise
-----

"Weißes" Rauschen hat angeblich eine gleichmäßige Leistung über das Frequenzspektrum. "Rosa" Rauschen ist exponentiell zugunsten niedrigerer Frequenzen verzerrt; gleiche Energie oder Anzahl von Sinusschwingungen pro Oktavband. Musikalische Oktaven sind exponentiell. Die Frequenzen 100 und 200 sind eine Oktave voneinander entfernt, ein Unterschied von 100. Aber eine Oktave über 2000 ist 4000, ein Unterschied von 2000. Weißes Rauschen repräsentiert Frequenzen gleichmäßig, daher gäbe es theoretisch 100 Frequenzen zwischen 100 und 200, dann wären es auch 2000 zwischen 2000 und 4000. Aber so hören wir keine Musik. Wir nehmen den Abstand zwischen 100 und 200 als gleich wahr wie zwischen 2000 und 4000. Rosa Rauschen passt die Verteilung der Frequenzen an, um dem zu entsprechen, wie wir hören. Also, wenn es 100 Frequenzen zwischen 100 und 200 gäbe (ein Oktavband), dann wären es nur 100 zwischen 2000 und 4000 (ein Oktavband). Rosa Rauschen klingt natürlicher und voller. Weißes Rauschen klingt heller und dünn. Wir können Rauschen durch additive Synthese erzeugen. Rosa Rauschen würde einen exponentiellen Zufallsprozess mit mehr tieferen Frequenzen erfordern. Die nachfolgenden Codezeilen zeigen eine lineare Zufallsreihe und eine exponentielle Reihe. Beachten Sie, dass bei der exponentiellen Reihe mehr niedrige Zahlen dargestellt sind. Anschließend werden diese beiden Funktionen verwendet, um 1000 zufällig ausgewählte Sinusschwingungen zu mischen und weißes und rosa Rauschen zu erzeugen. (Beachten Sie die CPU-Auslastung und vergleichen Sie sie mit dem einzelnen PinkNoise-Objekt.)

.. code-block::

   // Zuerst vergleichen Sie die Zufallszahlen und ihre Unterschiede.
   // Was ist auffällig?

   {rrand(1, 1000)}.dup(50).sort
   {exprand(1, 1000).round(1)}.dup(50).sort

   // Jetzt implementieren wir selbst weißes und rosa Rauschen.
   // Vergleichen Sie die CPU-Auslastung im Vergleich zu den
   // Implementierungen in Supercollider.

   // Weiß

   {Mix.fill(1000, {SinOsc.ar(rrand(1.0, 20000), mul: 0.01)})}.play
   {WhiteNoise.ar(mul: 0.2)}.play

   // Pink

   {Mix.fill(1000, {SinOsc.ar(exprand(1.0, 20000), mul: 0.01)})}.play
   {PinkNoise.ar(mul: 0.4)}.play

Hier sind einige Noise-generierende UGens in SuperCollider.
**ACHTUNG**: Die Lautstärke herunterdrehen oder die ``mul``-Argumente anpassen. Diese können laut sein! Beobachten Sie die Unterschiede im Server Scope:

.. code-block::

	{WhiteNoise.ar(mul: 1)}.scope(1)
	{PinkNoise.ar(mul: 1)}.scope(1)
	{BrownNoise.ar(mul: 1)}.scope(1)
	{GrayNoise.ar(mul: 1)}.scope(1)
	{Dust.ar(density: 100, mul: 1)}.scope(1)

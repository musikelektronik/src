=======================
Schwingungen und Wellen
=======================

Schwingungen

Beispiele für Schwingungen aus der alltäglichen Umgebung sind eine Schaukel, das Pendel einer Uhr, aber auch die Saite
einer Geige. Abstrakt lässt sich eine Schwingung beschreiben als eine periodische Hin- und Her-Bewegung. Es ist also
einer sich regelmäßig wiederholende Vorgang.

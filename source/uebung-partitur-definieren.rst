
Partitur definieren
====================

Im Folgenden entwerfen wir eine Art Partitur in Textform. Die Partitur, die eine einfache :code:`score.txt` Textdatei ist, definiert verschiedene Parameter für einen einzigen Klang. Jeder Klang mit seinen Parametern wird in einer Zeile angegeben (gefolgt von einem Zeilenumbruch). Bevor wir unsere Partitur abspielen können, ist es notwendig, ein Array gefüllt mit Sample-Buffers zu definieren, mit dem Namen :code:`~buffers`. Ein solches Array kann folgendermaßen definiert werden:

.. code-block::

    (
    ~paths = [
        "/home/amte/Desktop/speech.wav",
        "/home/amte/Desktop/glass.wav"
    ];
    ~buffers = Array.fill(~paths.size, {
        arg i;
        Buffer.readChannel(s, ~paths.at(i), channels: [0])
    })
    )

Jeder Klang besteht aus folgenden Parametern:

    * bufidx: Die Indexnummer des Sample-Buffers im :code:`~buffers`-Array, 0 ist das erste, 1 das zweite usw.
    * channel: Zu welchem Kanal der Klang geschickt werden soll. Für ein 2-kanaliges Stück ist 0 der linke und 1 der rechte Kanal.
    * atktime: Die Attack-Zeit des Abspiels, ausgedrückt in Sekunden.
    * reltime: Die Release-Zeit, also die Ausklingzeit des Samples, ebenfalls in Sekunden. Die Summe dieser beiden Parameter ergibt die Gesamtlänge des Klangs.
    * ratestart: Die Abspielgeschwindigkeit zu Beginn. 1 ist die Original-Samplegeschwindigkeit, 2 ist doppelt so schnell und eine Oktave höher usw.
    * rateend: Die Ziel-Abspielgeschwindigkeit. Es wird vom ratestart-Wert hierhin "gerutscht". Wenn die beiden Parameter unterschiedlich sind, entsteht eine Art Glissando. Gleiche Werte bedeuten kein Glissando.
    * clicks: Während der Dauer eines Klangs wird der Abspielvorgang des Klangs so oft von vorne wiederholt.
    * nextonset: Die Zeitdifferenz, gemessen vom Start des Abspiels, nach wie vielen Sekunden der nächste Klang einsetzen soll.

Die vollständige Definition kann :download:`hier <https://codeberg.org/musikelektronik/material/raw/main/scorereader.scd>` heruntergeladen werden.

Ein kurzes Partitur-Beispiel gibt es :download:`hier <https://codeberg.org/musikelektronik/material/raw/main/etude.txt>`.

Partitur-Generator in Python: :download:`Code <https://codeberg.org/musikelektronik/material/src/branch/main/scoregen.py>`

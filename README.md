# Build with script
From inside of the src folder run the `buildpg` script. This builds and updates both src and page repositories.

# How to build the page

1. Edit in src/source folder the final rst files
2. Build the html files. From the root `src/` folder run:

    ```
    sphinx-build -b html source/ build/html
    ``` 
3. Copy all `src/build/html/` files and folders into the pages repo:

    from the `src/` folder run
    ```
    cp -R build/html/* ../pages/ -v
    ```
4. Push changes to the origins


��^      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Sequenzierung�h]�h	�Text����Sequenzierung�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�D/home/amte/unterricht/muenchen/musikelektronik/src/source/ws-seq.rst�hKubh)��}�(hhh]�(h)��}�(h�Routine�h]�h�Routine�����}�(hh0hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh-hhhh,hKubh	�	paragraph���)��}�(hX�  Das Abspielen mehrerer Synth-Instanzen mit einfachen Iterationsmöglichkeiten wie ``do`` und ``collect`` führt zwangsläufig zu Akkorden, da wir mit ``do``\ /\ ``collect``\ -Konstruktionen keine Möglichkeit zum sequenziellen Abspielen der Synth-Instanzen haben (diese werden praktisch gleichzeitig ausgeführt). Mit der Klasse ``Routine`` können wir dies erreichen: Eine Routine ist im Grunde genommen eine Funktion, deren Ausführung wir mitten in der Aufführung für eine beliebige Zeit pausieren können.�h]�(h�RDas Abspielen mehrerer Synth-Instanzen mit einfachen Iterationsmöglichkeiten wie �����}�(hh@hhhNhNubh	�literal���)��}�(h�``do``�h]�h�do�����}�(hhJhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh@ubh� und �����}�(hh@hhhNhNubhI)��}�(h�``collect``�h]�h�collect�����}�(hh\hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh@ubh�. führt zwangsläufig zu Akkorden, da wir mit �����}�(hh@hhhNhNubhI)��}�(h�``do``�h]�h�do�����}�(hhnhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh@ubh�  /  �����}�(hh@hhhNhNubhI)��}�(h�``collect``�h]�h�collect�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh@ubh��  -Konstruktionen keine Möglichkeit zum sequenziellen Abspielen der Synth-Instanzen haben (diese werden praktisch gleichzeitig ausgeführt). Mit der Klasse �����}�(hh@hhhNhNubhI)��}�(h�``Routine``�h]�h�Routine�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh@ubh�� können wir dies erreichen: Eine Routine ist im Grunde genommen eine Funktion, deren Ausführung wir mitten in der Aufführung für eine beliebige Zeit pausieren können.�����}�(hh@hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubh?)��}�(hXB  Um eine Routine zu schreiben, müssen wir lediglich den Iterationscode innerhalb der Routine einsetzen und an der passenden Stelle bestimmen, wie lange unsere Routine nach jeder Iteration warten soll, bevor die nächste Iteration einsetzt. Dies geschieht mit Hilfe der ``wait``\ -Methode. Am Ende müssen wir unsere fertige Routine mithilfe der ``play``\ -Methode starten. Eine fertige Routine sieht folgendermaßen aus; hier spielen wir die ersten 20 Partialtöne der Grundfrequenz 110 Hz in einer Sequenz, wobei wir nach jedem Ton 150 Millisekunden warten (\ ``0.15.wait``\ ):�h]�(hX  Um eine Routine zu schreiben, müssen wir lediglich den Iterationscode innerhalb der Routine einsetzen und an der passenden Stelle bestimmen, wie lange unsere Routine nach jeder Iteration warten soll, bevor die nächste Iteration einsetzt. Dies geschieht mit Hilfe der �����}�(hh�hhhNhNubhI)��}�(h�``wait``�h]�h�wait�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh�ubh�D  -Methode. Am Ende müssen wir unsere fertige Routine mithilfe der �����}�(hh�hhhNhNubhI)��}�(h�``play``�h]�h�play�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh�ubh��  -Methode starten. Eine fertige Routine sieht folgendermaßen aus; hier spielen wir die ersten 20 Partialtöne der Grundfrequenz 110 Hz in einer Sequenz, wobei wir nach jedem Ton 150 Millisekunden warten (  �����}�(hh�hhhNhNubhI)��}�(h�``0.15.wait``�h]�h�	0.15.wait�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhh�ubh�  ):�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK
hh-hhubh	�literal_block���)��}�(h��(
Routine {
    (Array.series(20, 1) * 110).do {
        arg freq;
        freq.postln;
        Synth(\meinSynth, [freq: freq, dur: 0.5]);
        0.15.wait
    }
}.play
)�h]�h��(
Routine {
    (Array.series(20, 1) * 110).do {
        arg freq;
        freq.postln;
        Synth(\meinSynth, [freq: freq, dur: 0.5]);
        0.15.wait
    }
}.play
)�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��force���language��default��highlight_args�}�uh+h�hh,hKhh-hhubh?)��}�(hX%  **Übung** Verändern Sie die Parameter der obigen Routine, wie zum Beispiel das Array von Frequenzen, über das wir iterieren wollen. Passen Sie die Wartezeit der Routine an, sodass diese einen zufälligen Wert zwischen 100 Millisekunden und 500 Millisekunden annimmt (Stichwort ``rrand``\ ).�h]�(h	�strong���)��}�(h�
**Übung**�h]�h�Übung�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j	  hj  ubhX   Verändern Sie die Parameter der obigen Routine, wie zum Beispiel das Array von Frequenzen, über das wir iterieren wollen. Passen Sie die Wartezeit der Routine an, sodass diese einen zufälligen Wert zwischen 100 Millisekunden und 500 Millisekunden annimmt (Stichwort �����}�(hj  hhhNhNubhI)��}�(h�	``rrand``�h]�h�rrand�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj  ubh�  ).�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubh?)��}�(hX�  In der Tat kann man mit der ``next``\ -Methode den nächsten Wert einer Routine abrufen. Dies ermöglicht uns, auch unendliche Sequenzen nach bestimmten Mustern zu generieren. Im folgenden Beispiel definieren wir unsere Funktion aus dem Kapitel (?) zur Generierung der Fibonacci-Zahlen. Hierbei gestalten wir die Funktion jedoch so, dass wir mit einer ``inf.do`` eine unendliche Iteration starten (\ ``inf`` steht für Infinite und ist ein von SuperCollider definierter Begriff). Die unendliche Iteration wird jedoch hier durch ``yield`` unterbrochen. ``yield`` funktioniert synonym zu ``wait`` und gibt einen Zwischenwert der Funktion zurück:�h]�(h�In der Tat kann man mit der �����}�(hj5  hhhNhNubhI)��}�(h�``next``�h]�h�next�����}�(hj=  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj5  ubhX<    -Methode den nächsten Wert einer Routine abrufen. Dies ermöglicht uns, auch unendliche Sequenzen nach bestimmten Mustern zu generieren. Im folgenden Beispiel definieren wir unsere Funktion aus dem Kapitel (?) zur Generierung der Fibonacci-Zahlen. Hierbei gestalten wir die Funktion jedoch so, dass wir mit einer �����}�(hj5  hhhNhNubhI)��}�(h�
``inf.do``�h]�h�inf.do�����}�(hjO  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj5  ubh�& eine unendliche Iteration starten (  �����}�(hj5  hhhNhNubhI)��}�(h�``inf``�h]�h�inf�����}�(hja  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj5  ubh�y steht für Infinite und ist ein von SuperCollider definierter Begriff). Die unendliche Iteration wird jedoch hier durch �����}�(hj5  hhhNhNubhI)��}�(h�	``yield``�h]�h�yield�����}�(hjs  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj5  ubh� unterbrochen. �����}�(hj5  hhhNhNubhI)��}�(h�	``yield``�h]�h�yield�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj5  ubh� funktioniert synonym zu �����}�(hj5  hhhNhNubhI)��}�(h�``wait``�h]�h�wait�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj5  ubh�2 und gibt einen Zwischenwert der Funktion zurück:�����}�(hj5  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKhh-hhubh�)��}�(h��(
// Die Funktion zum Generieren der Fibonacci Sequenz
~fibGen = {
    var a=0, b=1;
    var tmpA = a;
    inf.do {
        a.yield;
        a = b;
        b = tmpA + b;
        tmpA = a;
    }
}
);

~fibs = Routine(~fibGen)�h]�h��(
// Die Funktion zum Generieren der Fibonacci Sequenz
~fibGen = {
    var a=0, b=1;
    var tmpA = a;
    inf.do {
        a.yield;
        a = b;
        b = tmpA + b;
        tmpA = a;
    }
}
);

~fibs = Routine(~fibGen)�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�j   �j  j  j  }�uh+h�hh,hKhh-hhubh?)��}�(h�ZMan kann nun mithilfe der ``next``\ -Methode die einzelnen Werte unserer Routine abfragen:�h]�(h�Man kann nun mithilfe der �����}�(hj�  hhhNhNubhI)��}�(h�``next``�h]�h�next�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj�  ubh�8  -Methode die einzelnen Werte unserer Routine abfragen:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK/hh-hhubh�)��}�(h��// Evaluieren Sie die folgende Zeile mehrmals
~fibs.next;
/*
-> 0
-> 1
-> 1
-> 2
-> 3
-> 5
-> 8
-> 13
-> 21
-> 34
-> 55
-> 89
-> 144
-> 233
-> 377
-> 610
-> 987
-> 1597
   usw.
*/�h]�h��// Evaluieren Sie die folgende Zeile mehrmals
~fibs.next;
/*
-> 0
-> 1
-> 1
-> 2
-> 3
-> 5
-> 8
-> 13
-> 21
-> 34
-> 55
-> 89
-> 144
-> 233
-> 377
-> 610
-> 987
-> 1597
   usw.
*/�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�j   �j  �supercollider�j  }�uh+h�hh,hK1hh-hhubh?)��}�(h�XEine Routine kann außerdem gestoppt werden, indem man die ``stop``\ -Methode verwendet:�h]�(h�;Eine Routine kann außerdem gestoppt werden, indem man die �����}�(hj�  hhhNhNubhI)��}�(h�``stop``�h]�h�stop�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj�  ubh�  -Methode verwendet:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKKhh-hhubh�)��}�(h�d// Nach der Evaluierung dieser Zeile hat unsere Routine keine Werte mehr zum Zurückgeben
~fibs.stop�h]�h�d// Nach der Evaluierung dieser Zeile hat unsere Routine keine Werte mehr zum Zurückgeben
~fibs.stop�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�j   �j  �supercollider�j  }�uh+h�hh,hKMhh-hhubh?)��}�(h�Aund sie kann durch die ``reset``\ -Methode zurückgesetzt werden:�h]�(h�und sie kann durch die �����}�(hj  hhhNhNubhI)��}�(h�	``reset``�h]�h�reset�����}�(hj&  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj  ubh�!  -Methode zurückgesetzt werden:�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hKRhh-hhubh�)��}�(h��// Durch die `reset`-Methode können wir immer wieder zum Anfang zurück springen.
// Evaluieren Sie die reset- und anschließend die next-Methode
~fibs.reset
~fibs.next�h]�h��// Durch die `reset`-Methode können wir immer wieder zum Anfang zurück springen.
// Evaluieren Sie die reset- und anschließend die next-Methode
~fibs.reset
~fibs.next�����}�hj>  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�j   �j  �supercollider�j  }�uh+h�hh,hKThh-hhubh?)��}�(hX�  Die Klasse ``Routine`` erbt die Methode ``play`` von der abstrakten Klasse ``Stream`` (eine Klasse, die wir nie direkt verwenden werden, sondern nur für die Definition weiterer Klassen vorgesehen ist). Die Methode ``play`` funktioniert im Wesentlichen wie das wiederholte Aufrufen der Methode ``next``\ , bis die Routine erschöpft ist und ``nil`` zurückgibt. Zu diesem Zeitpunkt wird das weitere Abrufen von Werten gestoppt. Ähnlich wie im oben genannten Fibonacci-Beispiel können wir den Fortschritt der Werteberechnungen in einer Schleife (wie beispielsweise ``do``\ ) verpacken und dabei entsprechende ``wait``\ - oder ``yield``\ -Anweisungen an geeigneten Stellen einfügen:�h]�(h�Die Klasse �����}�(hjN  hhhNhNubhI)��}�(h�``Routine``�h]�h�Routine�����}�(hjV  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh� erbt die Methode �����}�(hjN  hhhNhNubhI)��}�(h�``play``�h]�h�play�����}�(hjh  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh� von der abstrakten Klasse �����}�(hjN  hhhNhNubhI)��}�(h�
``Stream``�h]�h�Stream�����}�(hjz  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh�� (eine Klasse, die wir nie direkt verwenden werden, sondern nur für die Definition weiterer Klassen vorgesehen ist). Die Methode �����}�(hjN  hhhNhNubhI)��}�(h�``play``�h]�h�play�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh�G funktioniert im Wesentlichen wie das wiederholte Aufrufen der Methode �����}�(hjN  hhhNhNubhI)��}�(h�``next``�h]�h�next�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh�'  , bis die Routine erschöpft ist und �����}�(hjN  hhhNhNubhI)��}�(h�``nil``�h]�h�nil�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh�� zurückgibt. Zu diesem Zeitpunkt wird das weitere Abrufen von Werten gestoppt. Ähnlich wie im oben genannten Fibonacci-Beispiel können wir den Fortschritt der Werteberechnungen in einer Schleife (wie beispielsweise �����}�(hjN  hhhNhNubhI)��}�(h�``do``�h]�h�do�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh�&  ) verpacken und dabei entsprechende �����}�(hjN  hhhNhNubhI)��}�(h�``wait``�h]�h�wait�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh�	  - oder �����}�(hjN  hhhNhNubhI)��}�(h�	``yield``�h]�h�yield�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjN  ubh�/  -Anweisungen an geeigneten Stellen einfügen:�����}�(hjN  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK[hh-hhubh�)��}�(h��(
Routine {
    5.do {
        arg i;
        i.postln;
        "  +".postln;
        1.wait;
        "  -".postln;
        1.wait;
    };
    "Ende".postln
}.play
)

/*
-> a Routine
0
  +
  -
1
  +
  -
2
  +
  -
3
  +
  -
4
  +
  -
Ende
*/�h]�h��(
Routine {
    5.do {
        arg i;
        i.postln;
        "  +".postln;
        1.wait;
        "  -".postln;
        1.wait;
    };
    "Ende".postln
}.play
)

/*
-> a Routine
0
  +
  -
1
  +
  -
2
  +
  -
3
  +
  -
4
  +
  -
Ende
*/�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�j   �j  �supercollider�j  }�uh+h�hh,hK]hh-hhubh?)��}�(hX  oder wir können die Eingabewerte von ``yield`` als Wartezeit interpretieren: Wenn die Eingabe für ``yield`` eine Zahl ist, wird diese automatisch als Wartezeit interpretiert. Es wird so lange gewartet, bis der nächste Aufruf der ``next``\ -Methode stattfindet:�h]�(h�&oder wir können die Eingabewerte von �����}�(hj  hhhNhNubhI)��}�(h�	``yield``�h]�h�yield�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj  ubh�5 als Wartezeit interpretieren: Wenn die Eingabe für �����}�(hj  hhhNhNubhI)��}�(h�	``yield``�h]�h�yield�����}�(hj(  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj  ubh�{ eine Zahl ist, wird diese automatisch als Wartezeit interpretiert. Es wird so lange gewartet, bis der nächste Aufruf der �����}�(hj  hhhNhNubhI)��}�(h�``next``�h]�h�next�����}�(hj:  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj  ubh�  -Methode stattfindet:�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hh-hhubh�)��}�(h��(
Routine {
    0.25.postln.yield;
    0.5.postln.yield;
    1.postln.yield;
    2.postln.yield;
    4.postln.yield;
    "Ende".postln
}.play
)

/*
-> a Routine
0.25
0.5
1
2
4
Ende
*/�h]�h��(
Routine {
    0.25.postln.yield;
    0.5.postln.yield;
    1.postln.yield;
    2.postln.yield;
    4.postln.yield;
    "Ende".postln
}.play
)

/*
-> a Routine
0.25
0.5
1
2
4
Ende
*/�����}�hjR  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�j   �j  �supercollider�j  }�uh+h�hh,hK�hh-hhubeh}�(h!]��routine�ah#]�h%]��routine�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Task�h]�h�Task�����}�(hjm  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjj  hhhh,hK�ubh?)��}�(h�IEin Task ist größtenteils eine ähnliche Konstruktion wie eine Routine.�h]�h�IEin Task ist größtenteils eine ähnliche Konstruktion wie eine Routine.�����}�(hj{  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjj  hhubeh}�(h!]��task�ah#]�h%]��task�ah']�h)]�uh+h
hhhhhh,hK�ubh)��}�(hhh]�(h)��}�(h�Methodenvergleichstabelle�h]�h�Methodenvergleichstabelle�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh?)��}�(h��Die folgende Tabelle fasst einige der wichtigsten (gemeinsamen) Methoden der beiden Klassen zusammen. Für vollständige Informationen
konsultieren Sie bitte die Dokumentation der jeweiligen Klasse.�h]�h��Die folgende Tabelle fasst einige der wichtigsten (gemeinsamen) Methoden der beiden Klassen zusammen. Für vollständige Informationen
konsultieren Sie bitte die Dokumentation der jeweiligen Klasse.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  hhubh	�table���)��}�(hhh]�h	�tgroup���)��}�(hhh]�(h	�colspec���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]��colwidth�K!uh+j�  hj�  ubj�  )��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�j�  K!uh+j�  hj�  ubj�  )��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�j�  K!uh+j�  hj�  ubh	�thead���)��}�(hhh]�h	�row���)��}�(hhh]�(h	�entry���)��}�(hhh]�h?)��}�(h�Methodennamen�h]�h�Methodennamen�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubj�  )��}�(hhh]�h?)��}�(h�Routine�h]�h�Routine�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubj�  )��}�(hhh]�h?)��}�(h�Task�h]�h�Task�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubh	�tbody���)��}�(hhh]�(j�  )��}�(hhh]�(j�  )��}�(hhh]�h?)��}�(h�	``reset``�h]�hI)��}�(hjB  h]�h�reset�����}�(hjD  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj@  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj=  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj:  ubj�  )��}�(hhh]�h?)��}�(h�=Setzt den internen Zustand des Streams auf den Anfang zurück�h]�h�=Setzt den internen Zustand des Streams auf den Anfang zurück�����}�(hj`  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj]  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj:  ubj�  )��}�(hhh]�h?)��}�(h�Gleich wie bei **Routine**�h]�(h�Gleich wie bei �����}�(hjw  hhhNhNubj
  )��}�(h�**Routine**�h]�h�Routine�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j	  hjw  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjt  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj:  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj7  ubj�  )��}�(hhh]�(j�  )��}�(hhh]�h?)��}�(h�	``pause``�h]�hI)��}�(hj�  h]�h�pause�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubj�  )��}�(hhh]�h?)��}�(h�*Nicht vorhanden!*�h]�h	�emphasis���)��}�(hj�  h]�h�Nicht vorhanden!�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubj�  )��}�(hhh]�h?)��}�(h��Unterbricht momentan den Stream-Ablauf. Der nächste ``play``\ -Abruf nimmt den Vorgang dort, wo er abgebrochen wurde, wieder auf�h]�(h�5Unterbricht momentan den Stream-Ablauf. Der nächste �����}�(hj�  hhhNhNubhI)��}�(h�``play``�h]�h�play�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj�  ubh�D  -Abruf nimmt den Vorgang dort, wo er abgebrochen wurde, wieder auf�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj7  ubj�  )��}�(hhh]�(j�  )��}�(hhh]�h?)��}�(h�``stop``�h]�hI)��}�(hj  h]�h�stop�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj  ubj�  )��}�(hhh]�h?)��}�(h�Zerstört den Stream-Ablauf�h]�h�Zerstört den Stream-Ablauf�����}�(hj9  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj6  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj  ubj�  )��}�(hhh]�h?)��}�(h�Gleich wie ``pause``�h]�(h�Gleich wie �����}�(hjP  hhhNhNubhI)��}�(h�	``pause``�h]�h�pause�����}�(hjX  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhjP  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hjM  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj7  ubj�  )��}�(hhh]�(j�  )��}�(hhh]�h?)��}�(h�``play``�h]�hI)��}�(hj�  h]�h�play�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj~  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj{  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hjx  ubj�  )��}�(hhh]�h?)��}�(h��Evaluiert die Funktion der Klasse (das erste Argument) solange, bis sie ausgelaufen ist (d.h. nichts mehr zurückgeben kann bzw. ``nil`` zurückgegeben hat)�h]�(h��Evaluiert die Funktion der Klasse (das erste Argument) solange, bis sie ausgelaufen ist (d.h. nichts mehr zurückgeben kann bzw. �����}�(hj�  hhhNhNubhI)��}�(h�``nil``�h]�h�nil�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hHhj�  ubh� zurückgegeben hat)�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hjx  ubj�  )��}�(hhh]�h?)��}�(h�Gleich wie bei **Routine**�h]�(h�Gleich wie bei �����}�(hj�  hhhNhNubj
  )��}�(h�**Routine**�h]�h�Routine�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j	  hj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h>hh,hK�hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hjx  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj7  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+j5  hj�  ubeh}�(h!]�h#]�h%]�h']�h)]��cols�Kuh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  hhhNhNubeh}�(h!]��methodenvergleichstabelle�ah#]�h%]��methodenvergleichstabelle�ah']�h)]�uh+h
hhhhhh,hK�ubeh}�(h!]��sequenzierung�ah#]�h%]��sequenzierung�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�root_prefix��/��source_link�N�
source_url�N�toc_backlinks�j�  �footnote_backlinks���sectnum_xform���strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j;  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��de��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform���sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j  j  jg  jd  j�  j�  j  j  u�	nametypes�}�(j  �jg  �j�  �j  �uh!}�(j  hjd  h-j�  jj  j  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]�h	�system_message���)��}�(hhh]�h?)��}�(h��No directive entry for "list-table" in module "docutils.parsers.rst.languages.de".
Using English fallback for directive "list-table".�h]�h��No directive entry for „list-table“ in module „docutils.parsers.rst.languages.de“.
Using English fallback for directive „list-table“.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h>hj�  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type��INFO��line�K��source�h,uh+j�  hj�  hhhh,hK�uba�transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.
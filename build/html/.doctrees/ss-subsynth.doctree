��$      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Subtraktive Synthese�h]�h	�Text����Subtraktive Synthese�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�I/home/amte/unterricht/muenchen/musikelektronik/src/source/ss-subsynth.rst�hKubh	�	paragraph���)��}�(hX9  Beim subtraktiven Syntheseverfahren gehen wir den entgegengesetzten Weg zum additiven Syntheseverfahren. Wir formen unseren Klang, indem wir selektiv Daten aus einem komplexen Klangmaterial herausfiltern. Eine solche Klangquelle bildet das Rauschen. Zuerst betrachten wir die Charakteristik des Rauschens genauer.�h]�hX9  Beim subtraktiven Syntheseverfahren gehen wir den entgegengesetzten Weg zum additiven Syntheseverfahren. Wir formen unseren Klang, indem wir selektiv Daten aus einem komplexen Klangmaterial herausfiltern. Eine solche Klangquelle bildet das Rauschen. Zuerst betrachten wir die Charakteristik des Rauschens genauer.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Noise�h]�h�Noise�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh=hhhh,hKubh.)��}�(hXQ  "Weißes" Rauschen hat angeblich eine gleichmäßige Leistung über das Frequenzspektrum. "Rosa" Rauschen ist exponentiell zugunsten niedrigerer Frequenzen verzerrt; gleiche Energie oder Anzahl von Sinusschwingungen pro Oktavband. Musikalische Oktaven sind exponentiell. Die Frequenzen 100 und 200 sind eine Oktave voneinander entfernt, ein Unterschied von 100. Aber eine Oktave über 2000 ist 4000, ein Unterschied von 2000. Weißes Rauschen repräsentiert Frequenzen gleichmäßig, daher gäbe es theoretisch 100 Frequenzen zwischen 100 und 200, dann wären es auch 2000 zwischen 2000 und 4000. Aber so hören wir keine Musik. Wir nehmen den Abstand zwischen 100 und 200 als gleich wahr wie zwischen 2000 und 4000. Rosa Rauschen passt die Verteilung der Frequenzen an, um dem zu entsprechen, wie wir hören. Also, wenn es 100 Frequenzen zwischen 100 und 200 gäbe (ein Oktavband), dann wären es nur 100 zwischen 2000 und 4000 (ein Oktavband). Rosa Rauschen klingt natürlicher und voller. Weißes Rauschen klingt heller und dünn. Wir können Rauschen durch additive Synthese erzeugen. Rosa Rauschen würde einen exponentiellen Zufallsprozess mit mehr tieferen Frequenzen erfordern. Die nachfolgenden Codezeilen zeigen eine lineare Zufallsreihe und eine exponentielle Reihe. Beachten Sie, dass bei der exponentiellen Reihe mehr niedrige Zahlen dargestellt sind. Anschließend werden diese beiden Funktionen verwendet, um 1000 zufällig ausgewählte Sinusschwingungen zu mischen und weißes und rosa Rauschen zu erzeugen. (Beachten Sie die CPU-Auslastung und vergleichen Sie sie mit dem einzelnen PinkNoise-Objekt.)�h]�hXY  „Weißes“ Rauschen hat angeblich eine gleichmäßige Leistung über das Frequenzspektrum. „Rosa“ Rauschen ist exponentiell zugunsten niedrigerer Frequenzen verzerrt; gleiche Energie oder Anzahl von Sinusschwingungen pro Oktavband. Musikalische Oktaven sind exponentiell. Die Frequenzen 100 und 200 sind eine Oktave voneinander entfernt, ein Unterschied von 100. Aber eine Oktave über 2000 ist 4000, ein Unterschied von 2000. Weißes Rauschen repräsentiert Frequenzen gleichmäßig, daher gäbe es theoretisch 100 Frequenzen zwischen 100 und 200, dann wären es auch 2000 zwischen 2000 und 4000. Aber so hören wir keine Musik. Wir nehmen den Abstand zwischen 100 und 200 als gleich wahr wie zwischen 2000 und 4000. Rosa Rauschen passt die Verteilung der Frequenzen an, um dem zu entsprechen, wie wir hören. Also, wenn es 100 Frequenzen zwischen 100 und 200 gäbe (ein Oktavband), dann wären es nur 100 zwischen 2000 und 4000 (ein Oktavband). Rosa Rauschen klingt natürlicher und voller. Weißes Rauschen klingt heller und dünn. Wir können Rauschen durch additive Synthese erzeugen. Rosa Rauschen würde einen exponentiellen Zufallsprozess mit mehr tieferen Frequenzen erfordern. Die nachfolgenden Codezeilen zeigen eine lineare Zufallsreihe und eine exponentielle Reihe. Beachten Sie, dass bei der exponentiellen Reihe mehr niedrige Zahlen dargestellt sind. Anschließend werden diese beiden Funktionen verwendet, um 1000 zufällig ausgewählte Sinusschwingungen zu mischen und weißes und rosa Rauschen zu erzeugen. (Beachten Sie die CPU-Auslastung und vergleichen Sie sie mit dem einzelnen PinkNoise-Objekt.)�����}�(hhNhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK
hh=hhubh	�literal_block���)��}�(hX  // Zuerst vergleichen Sie die Zufallszahlen und ihre Unterschiede.
// Was ist auffällig?

{rrand(1, 1000)}.dup(50).sort
{exprand(1, 1000).round(1)}.dup(50).sort

// Jetzt implementieren wir selbst weißes und rosa Rauschen.
// Vergleichen Sie die CPU-Auslastung im Vergleich zu den
// Implementierungen in Supercollider.

// Weiß

{Mix.fill(1000, {SinOsc.ar(rrand(1.0, 20000), mul: 0.01)})}.play
{WhiteNoise.ar(mul: 0.2)}.play

// Pink

{Mix.fill(1000, {SinOsc.ar(exprand(1.0, 20000), mul: 0.01)})}.play
{PinkNoise.ar(mul: 0.4)}.play�h]�hX  // Zuerst vergleichen Sie die Zufallszahlen und ihre Unterschiede.
// Was ist auffällig?

{rrand(1, 1000)}.dup(50).sort
{exprand(1, 1000).round(1)}.dup(50).sort

// Jetzt implementieren wir selbst weißes und rosa Rauschen.
// Vergleichen Sie die CPU-Auslastung im Vergleich zu den
// Implementierungen in Supercollider.

// Weiß

{Mix.fill(1000, {SinOsc.ar(rrand(1.0, 20000), mul: 0.01)})}.play
{WhiteNoise.ar(mul: 0.2)}.play

// Pink

{Mix.fill(1000, {SinOsc.ar(exprand(1.0, 20000), mul: 0.01)})}.play
{PinkNoise.ar(mul: 0.4)}.play�����}�hh^sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��force���language��default��highlight_args�}�uh+h\hh,hKhh=hhubh.)��}�(h��Hier sind einige Noise-generierende UGens in SuperCollider.
**ACHTUNG**: Die Lautstärke herunterdrehen oder die ``mul``-Argumente anpassen. Diese können laut sein! Beobachten Sie die Unterschiede im Server Scope:�h]�(h�<Hier sind einige Noise-generierende UGens in SuperCollider.
�����}�(hhshhhNhNubh	�strong���)��}�(h�**ACHTUNG**�h]�h�ACHTUNG�����}�(hh}hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h{hhsubh�*: Die Lautstärke herunterdrehen oder die �����}�(hhshhhNhNubh	�literal���)��}�(h�``mul``�h]�h�mul�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hhsubh�^-Argumente anpassen. Diese können laut sein! Beobachten Sie die Unterschiede im Server Scope:�����}�(hhshhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK"hh=hhubh])��}�(h��{WhiteNoise.ar(mul: 1)}.scope(1)
{PinkNoise.ar(mul: 1)}.scope(1)
{BrownNoise.ar(mul: 1)}.scope(1)
{GrayNoise.ar(mul: 1)}.scope(1)
{Dust.ar(density: 100, mul: 1)}.scope(1)�h]�h��{WhiteNoise.ar(mul: 1)}.scope(1)
{PinkNoise.ar(mul: 1)}.scope(1)
{BrownNoise.ar(mul: 1)}.scope(1)
{GrayNoise.ar(mul: 1)}.scope(1)
{Dust.ar(density: 100, mul: 1)}.scope(1)�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]�hlhmhn�hohphq}�uh+h\hh,hK%hh=hhubeh}�(h!]��noise�ah#]�h%]��noise�ah']�h)]�uh+h
hhhhhh,hKubeh}�(h!]��subtraktive-synthese�ah#]�h%]��subtraktive synthese�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�root_prefix��/��source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks���sectnum_xform���strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�h�error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��de��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform���sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(h�h�h�h�u�	nametypes�}�(hŉh��uh!}�(h�hh�h=u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.
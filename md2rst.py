from m2r import parse_from_file

mdfile = "ss-subsynth.md"
rstfile = mdfile.split(".")[0] + ".rst"
output = parse_from_file(mdfile)
with open(f"source/{rstfile}", "w") as rst:
    rst.write(output)
